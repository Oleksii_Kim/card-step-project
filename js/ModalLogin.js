class Modal extends HtmlElement {
    myModal = null;


    constructor({classes, title, content}) {
        super(classes);
        this.title = title;
        this.content = content;
    }

    render(container) {
        this.createModal();
        this.insertIntoPage(container);
        this.submitForm();
        this.openModal();
    }

    createModal() {
        const modalLayout = `
      <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Login</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form class="row g-3">
                    <div class="col-auto">
                        <label class="visually-hidden">Email</label>
                        <input type="email" class="form-control" id="inputEmail" placeholder="Email">
                    </div>
                    <div class="col-auto">
                        <label  class="visually-hidden">Password</label>
                        <input type="password" class="form-control" id="inputPassword2" placeholder="Password">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button id="buttonAccessLogin" type="button" class="btn btn-primary">Login</button>
            </div>
        </div>
    </div>
</div>
      `;
        this.createElement({
            element: "div",
            attributes: {
                innerHTML: modalLayout,
            },
        });
        this.content.render(this.element);
    }

    getFormValue() {
        const email = document.getElementById('inputEmail');
        const password = document.getElementById('inputPassword2');

        return {
            email: email.value,
            password: password.value
        };
    }

    submitForm() {
        const buttonLogin = document.getElementById('buttonAccessLogin');
        buttonLogin.addEventListener("click", e => {
            const {email, password} = this.getFormValue();
            Login(email, password)
                .then(token => {
                    if (token !== 'Incorrect username or password') {
                        this.closeModal();
                        changeButton(token)
                    }
                });
        });
    }
    openModal() {
        this.myModal = new bootstrap.Modal(document.getElementById('exampleModal'));
        this.myModal.show();
    }

    closeModal() {
        this.myModal.hide();
    }

    insertIntoPage(container = document.body) {
        container.append(this.element);
    }
}
