class HtmlElement {
  constructor(classes) {
    this.classes = classes;
    this.element = null;
  }
  createElement(elementData) {
    this.element = document.createElement(elementData.element);
    this.element.className = this.classes.join(" ");
    Object.keys(elementData.attributes).forEach((key) => {
      this.element[key] = elementData.attributes[key];
    });
  }

  insertIntoPage(container) {
    container.append(this.element);
  }
}
