const buttonLogin = document.getElementById('buttonLogin');
const buttonVisit = document.getElementById('buttonVisit');
buttonLogin.addEventListener("click", elem => {
    const modal = new Modal({
        classes: ['class'], title: 'title', content: {
            render: () => {
                modal.insertIntoPage();
            }
        }
    });
    modal.render();
});

function changeButton(token) {
    if (token) {
        buttonLogin.style.display = 'none';
        buttonVisit.style.display = 'block';
    }
}